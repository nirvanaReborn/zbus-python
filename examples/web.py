#encoding=utf8   
import sys
sys.path.append("../") 

from zbus import RpcProcessor, RpcServer, Message, Protocol, StaticResource
from zbus import route, add_filter, redirect, exclude
from zbus import Template

template = Template(cache_enabled=True) #callable

def auth(req, res):
    print(req)
    return True
 
class MyService:   
    @route('/')
    def home(self):
        return template('index.html', name='hong')
     
    def p(self, name, age=10, address='sz'):
        return {
            'name': name,
            'age': int(age),
            'address': address
        }
    
    def plus(self, a, b): 
        return int(a) + int(b) #Non-Message type, return as json to client
     
    def req(self, req):
        print(req)
    
    @add_filter(auth)
    def msg(self):
        return Message(200, 'message 200 body') 
    
    def bing(self):
        return redirect('http://bing.com')
    
    @exclude()
    def redirect(self):
        pass
        
    
'''
侵入代码部分，从zbus上取消息处理返回
'''

p = RpcProcessor() 
p.mount('/', MyService())
p.mount('/static', StaticResource(base_dir='/tmp'))  
 
    
server = RpcServer(p) 
#需要认证的时候打开
server.enable_auth('2ba912a8-4a8d-49d2-1a22-198fd285cb06', '461277322-943d-4b2f-b9b6-3f860d746ffd') #apiKey + secretKey
server.mq_server_address = 'localhost:80' 
server.mq = 'abc' #start with 
server.use_thread = False
server.mq_mask = Protocol.MASK_DELETE_ON_EXIT#delete on exit
server.start()
