#encoding=utf8   
from __future__ import print_function

import sys
sys.path.append("../")  

from zbus import MqClient, Message, Protocol

mq = 'MyMQ'
channel = 'MyChannel'
 
def onopen(client):  
    msg = Message()
    msg.headers.cmd = 'create'
    msg.headers.mq = mq 
    msg.headers.mqMask = Protocol.MASK_DELETE_ON_EXIT
    #msg.headers['mqType'] = 'disk' #default to memory
    msg.headers.channel = channel  
    res = client.invoke(msg) 
    print(res)
    
    msg = Message()
    msg.headers.cmd = 'sub'
    msg.headers.mq = mq 
    #msg.headers['filter'] = 'abc'
    msg.headers.channel = channel 
    msg.headers.window = 1  #set in-flight message count 
     
    res = client.invoke(msg)
    print(res)

def message_handler(msg):
    print(msg)

client = MqClient('zbus.io')
client.onopen = onopen
client.add_mq_handler(mq=mq, channel=channel, handler=message_handler)
client.connect() 

