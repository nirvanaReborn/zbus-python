#encoding=utf8   
import sys
sys.path.append("../") 

from zbus.db import Db    

                
from sqlalchemy import create_engine                   
                
engine = create_engine("mysql+pymysql://root:root@localhost/test?charset=utf8")

db = Db(engine)


sql = "insert into `user`(name,age) values(:name, :age)"
sql = "update `user` set name=:name, age=:age where id=:id"
users = [
    {'id': 1, 'name': 'yhong2', 'age': 12},
    {'id': 2, 'name': 'yhong22', 'age': 18}
]

with db.session() as s:
    res = db.update_many('user', users, session=s)
    print(res)
